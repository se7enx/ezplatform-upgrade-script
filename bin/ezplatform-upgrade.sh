#!/usr/bin/env bash

EZ_REMOTE_NAME="ez"
EZ_REMOTE_REPO_URL="git@github.com:ezsystems/ezplatform.git"

EZ_VERSION=${1-3.0.0}
MERGE_STRATEGY=${2-"patience"}

check-project-git-repo() {
    if [ ! -d .git ] ; then
        echo "[ERROR] Current directory is not a project root directory"
        exit 1
    fi
}

add-ez-remote() {
    tmp=$(git remote -v | grep "${EZ_REMOTE_REPO_URL}" | awk '{print $1}' | grep -e "^${EZ_REMOTE_NAME}$" | wc -l)
    if [ "$tmp" -gt 0 ] ; then
        return
    fi

    git remote add ${EZ_REMOTE_NAME} ${EZ_REMOTE_REPO_URL}
}

fetch-ez-remote() {
    git fetch ${EZ_REMOTE_NAME}
    git fetch ${EZ_REMOTE_NAME} --tags
}

check-version-tag() {
    VERSION_TAG="v${1}"
    tmp=$(git tag -l "${VERSION_TAG}" | wc -l)
    if [ "$tmp" -eq 0 ] ; then
        echo "[ERROR]: Versions ${VERSION_TAG} does not exist"
        exit 1
    fi
}

create-upgrade-branch() {
    BRANCH_NAME="upgrade-${1}"

    git checkout master
    git fetch origin
    git pull origin master

    tmp=$(git branch | grep "${BRANCH_NAME}" | wc -l)
    if [ "$tmp" -eq 0 ] ; then
        git checkout -b ${BRANCH_NAME}
    else
        git checkout ${BRANCH_NAME}
        git pull origin ${BRANCH_NAME}
    fi
}

merge-version-tag() {
    VERSION_TAG="v${1}"
    STRATEGY=$2

    git reset --hard HEAD
    git merge -X ${STRATEGY} ${VERSION_TAG}
}

check-project-git-repo
add-ez-remote
fetch-ez-remote
check-version-tag $EZ_VERSION
create-upgrade-branch $EZ_VERSION
merge-version-tag $EZ_VERSION $MERGE_STRATEGY